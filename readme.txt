########################################################
Info about files and folders inside of downloaded folder
########################################################

1. input
>> This folder includes design resources like psd, sketch, fonts and so on.

2. markup
>> This folder includes markup files.


#####################################
Markup Options / Project requirements
#####################################
Framework - Bootstrap 3
Responsive - At own discretion
Fonts - Google font
CSS preprocessors - SASS
JQuery implementation - Yes
Web accessibility (WCAG 2.0 (AA) / Section 508) - Yes
Pixel perfect - Yes
Commented Markup - Yes
HTML5 - Yes
CSS3 - Yes


Looking forward to work with you!